const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const ratingModule = require("./modules/rating.module");
const commentModule = require("./modules/comment.module");

const app = express();

//Port number
const port =  process.env.PORT || 8080;

app.use((req, res, next)=>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


//Set static folder
app.use(express.static(path.join(__dirname,'public')));

app.use(bodyParser.urlencoded({
    extended: true
}));

app.post('/api/rating',(req,res,next)=>{
    let movieId = req.body.movieId;
    let rate = req.body.rate;
    if(movieId !== "" && rate!== "") {
        ratingModule.saveRating({movieId,rate})
        res.json( {success:true});
    }else {
       res.json({sucess:false,message:'Invalid data'});
    }
});

app.get('/api/rating/:movieId',(req,res,next)=>{
    res.json(ratingModule.getRatingByMovie(req.params.movieId))
});

app.post('/api/comment',(req,res,next)=>{
    let movieId = req.body.movieId;
    let name = req.body.name;
    let date = req.body.date;
    let comment = req.body.comment;
    if(movieId !== "" && name!== "" && date !=="" && comment !== "") {
        commentModule.addComment({movieId,name,date,comment});
        res.json( {success:true});
    }else {
       res.json({sucess:false,message:'Invalid data'});
    }
});

app.get('/api/comment/:movieId',(req,res,next)=>{
    res.json(commentModule.getCommentsByMovie(req.params.movieId));
});


app.get('*',(req,res)=> {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});
app.listen(port,() => {
    console.log('Server started on port '+port);
});