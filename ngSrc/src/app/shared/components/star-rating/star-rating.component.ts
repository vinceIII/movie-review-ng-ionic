import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RatingService } from '../../services/rating.service';
import { Subscription } from 'rxjs/Subscription';
;

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnInit {
  
  starRatings = [1,2,3,4,5];
  hoveredRating:number = 0;
  submitted:boolean = false;
  getRatingSubscrition:Subscription;
  postRatingSubsciption:Subscription;

  @Input() imdbID;
  @Output() updateRating:EventEmitter<any> = new EventEmitter();

  constructor(private _ratingService:RatingService) { }

  ngOnInit() {}
  clearStars() {
    if(!this.submitted) this.hoveredRating = 0;
  }
  onStarHover(e) {
    if(!this.submitted) this.hoveredRating = e;
  }
  submitRating(rating) {
    let data = {'movieId':this.imdbID,rate:rating};
    if(!this.submitted) {
    this.postRatingSubsciption =  this._ratingService.postRating(data).subscribe(response=>{
        if(response.body.success) {
          this.submitted = true;
          this.hoveredRating = rating;
        this.getRatingSubscrition =  this._ratingService.getRating(this.imdbID).subscribe(resRating=>{
            if(resRating.data.rating) {
              this.updateRating.emit(resRating.data.rating);
            } 
          });
        }
      })
    }
  }
}
