import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommentService } from '../../services/comment.service';
import {  } from 'protractor';

@Component({
  selector: 'app-post-comment-form',
  templateUrl: './post-comment-form.component.html',
  styleUrls: ['./post-comment-form.component.scss']
})
export class PostCommentFormComponent implements OnInit {
  
  commentForm:FormGroup;
  username:string = `Guest123`;

  @Input() imdbID;
  @Output() addComment:EventEmitter<any> = new EventEmitter();
  isSubmitting:boolean = false;

  constructor(private _commentService:CommentService) { }

  ngOnInit() {
    this.commentForm = new FormGroup({
      comment: new FormControl('',Validators.required)
    })
  }
  
  postDate() {
    var currentdate = new Date(); 
    var date = `${currentdate.getDate()}-${(currentdate.getMonth()+1)}-${currentdate.getFullYear()}`;
    var hours = currentdate.getHours();
    var minutes = currentdate.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; 
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return `${date} : ${strTime}`;
  }

  onSubmit() {
    if(this.commentForm.status === 'VALID' && this.imdbID) {
      let data = {
        movieId: this.imdbID,
        name: this.username,
        date: this.postDate(),
        comment: this.commentForm.value.comment
      };
      this.isSubmitting = true;
      this._commentService.postCommnent(data).subscribe(res=>{
        if(res.body.success === true) {
          this.isSubmitting = false;
          this.addComment.emit(data);
          this.commentForm.reset();
        }
      });
    } 
  }


}
