import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { Observable } from 'rxjs/Observable';



@Injectable()
export class MovieService {

  apiKey = `a5ce9d09`;
  movieApi = `http://www.omdbapi.com/?apikey=${this.apiKey}`;
  
  constructor(private _http:HttpClient) { }

  searchMovies(keyword):Observable<any> {
    return this._http.get(`${this.movieApi}&s=${keyword}`).pipe(map(result=>result));
  }

  getMovie(imdbID) {
    return this._http.get(`${this.movieApi}&i=${imdbID}`).pipe(map(result=>result));
  }

}
