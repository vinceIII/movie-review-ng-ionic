import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

@Injectable()
export class CommentService {

  apiUrl = `http://localhost:8080/api/comment`;
  constructor(private _http:HttpClient) { }
  
  postCommnent(data):Observable<any> {
    const body = new HttpParams()
          .set(`movieId`, data.movieId)
          .set(`name`, data.name)
          .set(`date`, data.date)
          .set(`comment`, data.comment);
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this._http.post(this.apiUrl, body.toString(),{ headers, observe: 'response' }).pipe(map(res=>res));
  }
  
  getComments(movieId):Observable<any> {
    return this._http.get(`${this.apiUrl}/${movieId}`,{}).pipe(map(res=>res));
  }

}
