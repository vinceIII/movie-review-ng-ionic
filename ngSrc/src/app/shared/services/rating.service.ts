import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class RatingService {

  apiUrl = `http://localhost:8080/api/rating`;

  constructor(private _http:HttpClient) { }

  postRating(data):Observable<any> {
    const body = new HttpParams()
          .set(`movieId`, data.movieId)
          .set(`rate`, data.rate);
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this._http.post(this.apiUrl, body.toString(),{ headers, observe: 'response' }).pipe(map(res=>res));
  }

  getRating(movieId):Observable<any> {
    return this._http.get(`${this.apiUrl}/${movieId}`,{}).pipe(map(res=>res));
  }

}
