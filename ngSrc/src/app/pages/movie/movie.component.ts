import { Component, OnInit, Output, Input, OnDestroy } from '@angular/core';
import { Events, NavParams, IonicPage, LoadingController } from 'ionic-angular';
import { MovieService } from '../../shared/services/movie.service';
import { RatingService } from '../../shared/services/rating.service';
import { CommentService } from '../../shared/services/comment.service';
import { Subscription } from 'rxjs/Subscription';

@IonicPage()
@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit,OnDestroy {

  title:string = "Movie";
  year:string = "";
  movie:any;
  rating:Number = 0;
  comments = [];
  @Output() imdbID;
  @Output() commetList;

  movieServiceSubscription:Subscription;
  ratingServiceSubscription:Subscription;
  commentServiceSubscription:Subscription;

  constructor(
    private _movieService:MovieService,
    private _ratingService:RatingService,
    private _commentService:CommentService,
    public events:Events,
    public navParams: NavParams,
    public  loaderCtrl:LoadingController
  ) {
    this.title = this.navParams.get('Title');
    this.year = this.navParams.get('Year');
  }

  ngOnInit() {
    let loading = this.loaderCtrl.create({
      content: 'Please wait...'
    });
    loading._preLoad();

   this.movieServiceSubscription = this._movieService.getMovie(this.navParams.get('imdbID'))
      .subscribe(result=>{
        if(result) {
          this.movie = result;
        this.ratingServiceSubscription =  this._ratingService.getRating(this.movie.imdbID).subscribe(resRating=>{
            console.log('resRating',resRating);
            if(resRating.data.rating) {
              this.rating = resRating.data.rating;
            } 
          });
        this.commentServiceSubscription =  this._commentService.getComments(this.movie.imdbID).subscribe(res=>{
            this.comments = res;
            console.log(this.comments);
          });

        }
    });
  }
  ngOnDestroy() {
    this.movieServiceSubscription.unsubscribe();
    this.ratingServiceSubscription.unsubscribe();
    this.commentServiceSubscription.unsubscribe();
  }
  updateRatingData(newRating) {
    this.rating = newRating;
  }
  updateComments(newComment){
    this.comments.push(newComment);
  }

}
