import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicApp, IonicModule, NavController, Events, Config, App, Platform, DomController, Keyboard } from 'ionic-angular'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';


import { HomeAppComponent } from './home-app.component';
import { AppComponent } from '../../app.component';
import { MovieService } from '../../shared/services/movie.service';

describe('HomeAppComponent', () => {
  let component: HomeAppComponent;
  let fixture: ComponentFixture<HomeAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [IonicModule,HttpClientTestingModule],
      declarations: [ AppComponent,HomeAppComponent ],
      providers:[NavController,MovieService,Events,Config,App,Platform,DomController,Keyboard,IonicApp]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
