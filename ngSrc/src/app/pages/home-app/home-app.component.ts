import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { MovieComponent } from '../movie/movie.component';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { debounceTime } from 'rxjs/operators';
import { MovieService } from '../../shared/services/movie.service';

// @IonicPage()
@Component({
	selector: 'app-home-app',
	templateUrl: './home-app.component.html',
	styleUrls: ['./home-app.component.scss']
})

export class HomeAppComponent implements OnInit,OnDestroy {
	searchQuery: string = '';
	items: string[];
	movies:any = [];
	defualtMovieList:any = [];
	movieServiceSubscription: Subscription;
	
	defaulMovieKeyword = ['world','marvel','thor','alien','superman','ironman','ant',
						'wild','summer','winter','sassy','action','drama','suspense','star wars','star trek','space','random'];



	private keypress = new Subject();
	private subscription: Subscription;

	constructor(
		private navCtrl:NavController,
		private _movieService:MovieService,
		public events:Events
	){}

	ngOnInit() {
		this.getDefaultMovie();
		this.movies = this.defualtMovieList
		this.subscription = this.keypress.pipe(
		debounceTime(800))
			.subscribe(e => {
				if(!e) {
					this.movies = this.defualtMovieList
				}else {
					this.getMovies(e);
				}
		});
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
		this.movieServiceSubscription.unsubscribe();
	}

	getItems(ev: any) {
		const val = ev.target.value;
		this.keypress.next(val);
	}

	getDefaultMovie() {
		this.movieServiceSubscription =	this._movieService.searchMovies(this.defaulMovieKeyword[Math.floor(Math.random() * 17) + 1]).subscribe(
			result=>{
				this.defualtMovieList = result.Search;
				this.movies = result.Search;
			}
		)
	}

	getMovies(keyword) {
		this.movieServiceSubscription =	this._movieService.searchMovies(keyword).subscribe(
			result=>{
				this.movies	= result.Search
			}
		)
	}

	gotoMovie(movie) {
		this.navCtrl.push(MovieComponent,movie);
	}

}
