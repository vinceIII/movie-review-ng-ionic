import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicModule } from 'ionic-angular';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeAppComponent } from './pages/home-app/home-app.component';
import { MovieComponent } from './pages/movie/movie.component';
import { MovieService } from './shared/services/movie.service';
import { StarRatingComponent } from './shared/components/star-rating/star-rating.component';
import { RatingService } from './shared/services/rating.service';
import { PostCommentFormComponent } from './shared/components/post-comment-form/post-comment-form.component';
import { CommentService } from './shared/services/comment.service';
import { CommentListComponent } from './shared/components/comment-list/comment-list.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeAppComponent,
    MovieComponent,
    StarRatingComponent,
    PostCommentFormComponent,
    CommentListComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(AppComponent),
    HttpClientModule,
    FormsModule
  ],
  providers: [MovieService,RatingService,CommentService],
  bootstrap: [AppComponent],
  entryComponents: [
    HomeAppComponent,
    AppComponent,
    MovieComponent
  ],
})
export class AppModule { }
