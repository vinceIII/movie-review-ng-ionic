const fs = require("fs");

var getjsonData = ()=>{
    try {
        return  JSON.parse(fs.readFileSync('./data/rating.json'));
    } catch (error) {
        return [];
    }
}

var getRatingByMovie = (id)=>{
    try {
        let data = getjsonData();
        let result;
        let total = 0;
        if(data) {
           result = data.filter(movie=>movie.movieId == id)
        }
        if(result) {
          result.forEach(movie => {
              total = parseInt(total) + parseInt(movie.rate);
          });
        }
        rating = (total/result.length).toFixed(1);
        return {success: true, data:{
            rating:parseFloat(rating)
        }};
    } catch (error) {
        return [];
    }
}

var saveRating = (newData)=> {
   let data =  getjsonData();
   data.push(newData);
   fs.writeFileSync('./data/rating.json',JSON.stringify(data),function(err){
        if(err){
            return false;
        }else {
            return true;
        }
    });
}

module.exports = {
    getRatingByMovie,
    saveRating
};