const fs = require("fs");

var getjsonData = ()=>{
    try {
        return  JSON.parse(fs.readFileSync('./data/comments.json'));
    } catch (error) {
        return [];
    }
}

var getCommentsByMovie = (id)=> {
    try {
        let data = getjsonData();
        let result;
        let total = 0;
        if(data) {
           result = data.filter(movie=>movie.movieId == id)
        }
            return result;
    } catch (error) {
        return [];
    }
}

var addComment = (newData)=> {
    let data =  getjsonData();
    data.push(newData);
    fs.writeFileSync('./data/comments.json',JSON.stringify(data),function(err){
         if(err){
             return false;
         }else {
             return true;
         }
     });
}

module.exports = {
    getCommentsByMovie,
    addComment
}